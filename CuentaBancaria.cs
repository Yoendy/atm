﻿using ATM.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using atm;

namespace ATM
{
    class CuentaBancaria
    {
        private ATMContext context;
        private int cuentaId;
        public CuentaBancaria(int cuentaId)
        {
            context = new ATMContext();
            this.cuentaId = cuentaId;
        }
        public decimal GetBalance()
        {
            var cuenta = context.Cuentas.FirstOrDefault(x => x.Id == this.cuentaId);

            return cuenta.Balance;
        }

        public decimal Depositar(decimal monto)
        {
            var cuenta = context.Cuentas.FirstOrDefault(x => x.Id == this.cuentaId);
            cuenta.Balance += monto;
            context.SaveChanges();

            var cajero = new CajeroBancario();
            cajero.Depositar(monto);

            return cuenta.Balance;
        }

		public decimal Retirar(decimal monto)
        {
            var cuenta = context.Cuentas.FirstOrDefault(x => x.Id == this.cuentaId);
            cuenta.Balance -= monto;
            context.SaveChanges();

            var cajero = new CajeroBancario();
			cajero.Retirar(monto);

            return cuenta.Balance;
        }
    }
}
