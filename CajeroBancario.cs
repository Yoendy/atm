﻿using ATM.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace atm
{
    public class CajeroBancario
    {
        private int cajeroId = 1;
        private ATMContext context;

        public CajeroBancario()
        {
            context = new ATMContext();
        }

		public bool VerificarMontoDisponible(decimal monto )
		{
			var cajero = context.Cajeros.FirstOrDefault(x => x.Id == cajeroId);
	
			return cajero.MontoDisponible >= monto;
		}

        public bool PermiteDeposito()
        {
            var cajero = context.Cajeros.FirstOrDefault(x => x.Id == cajeroId);
			return !cajero.Atascado ;
        }
   
        public bool PermiteRetiro()
        {
            var cajero = context.Cajeros.FirstOrDefault(x => x.Id == cajeroId);
            return !cajero.Atascado;
        }

        public void Depositar(decimal monto)
        {
            var cuenta = context.Cajeros.FirstOrDefault(x => x.Id == this.cajeroId);
            cuenta.MontoDisponible += monto;
            context.SaveChanges();

        }

		public void Retirar(decimal monto)
        {
            var cuenta = context.Cajeros.FirstOrDefault(x => x.Id == this.cajeroId);
            cuenta.MontoDisponible -= monto;
            context.SaveChanges();

        }
    }
}
