using System;
using ATM.Service;
using System.Linq;
using System.Linq.Expressions;

namespace ATM
{
    class Tarjeta
    {
        private string tarjeta;
        private ATMContext context;
        public Tarjeta(string tarjeta)
        {
            this.tarjeta = tarjeta;
            context = new ATMContext();
        }
        public bool EsValida()
        {
            //Validar Tarjeta Esta en Archivo
            return context.Tarjetas.Where(x => x.PAN == this.tarjeta).Any();
        }
        public bool EsElPINCorrecto(string pin)
        {
            //Validar número de identificación personal (PIN)
            return context.Tarjetas.Any(x => x.PAN == this.tarjeta && x.PIN == pin);
        }

        public int GetCuentaId()
        {
            return context.Tarjetas.FirstOrDefault(x => x.PAN == this.tarjeta).CuentaId;
        }
 
    }
}