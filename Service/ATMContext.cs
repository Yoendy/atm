using System;
using ATM.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace ATM.Service
{
    public class ATMContext : DbContext
    {
        public DbSet<DomainModel.Tarjeta> Tarjetas { get; set; }
        public DbSet<Cuenta> Cuentas { get; set; }
        public DbSet<Cajero> Cajeros { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Filename=./ATM.sqlite");
        }
    }
}
