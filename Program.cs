﻿using atm;
using System;

namespace ATM
{
	static class Program
	{
		private static Tarjeta card;
		private static CuentaBancaria cuentaBancaria;
		private static CajeroBancario cajero = new CajeroBancario();

		static void Main(string[] args)
		{   //SCREEN 1
			Console.WriteLine("Bienvenido, por favor inserte su tarjeta!");

			var pan = Console.ReadLine();

			try
			{

				card = new Tarjeta(pan);


				if (card.EsValida())
				{
					cuentaBancaria = new CuentaBancaria(card.GetCuentaId());
					ValidCard();
				}
				else
				{
					InvalidCard();
				}

			}

			catch (Exception ex)
			{

				Console.WriteLine("Disculpe, se presento algun error");
			}
		}

		static void InvalidCard()
		{
			//SCREEN 4
			Console.WriteLine("Tarjeta invalida, ha sido retenida.");
			return;
		}

		static void ValidCard()
		{
			//SCREEN 2
			int intentosErroneos = 0;
			Console.WriteLine("Por favor digite su PIN");
			var pin = Console.ReadLine();

			if (card.EsElPINCorrecto(pin))
			{
				DisplayTransaction();
			}

			while (!card.EsElPINCorrecto(pin) && intentosErroneos < 3)
			{
				intentosErroneos += 1;
				if (intentosErroneos == 3)
					InvalidCard();
				else
				{
					//SCREEN 3
					Console.WriteLine("El PIN es incorrecto, por favor intente de nuevo.");
					pin = Console.ReadLine();
				}
				if (card.EsElPINCorrecto(pin))
					DisplayTransaction();
			}



		}

		static void DisplayTransaction()
		{
			//SCREEN 5
			Console.WriteLine("Selecciona la transaccion.\n1. Balance \n2. Deposito \n3. Retiro");
			SeleccionarTransaccion();
			return;
		}

		static void SeleccionarTransaccion()
		{
			var option = Console.ReadLine();

			switch (option)
			{
				case "1":
					DisplayBalance();
					break;

				case "2":
					RealizarDeposito();
					break;

				case "3":
					RealizarRetiro();
					break;

				default:
					break;
			}
		}



		static void AnotherTransation()
		{
			//SCREEN 14
			Console.WriteLine("¿Desea realizar otra transacción? \n 1. Si\n 2. No");

			var option = Console.ReadLine();

			switch (option)
			{
				case "1":
					DisplayTransaction();
					break;

				case "2":
					RetirarTarjeta();
					break;
			}
		}

		static void DisplayBalance()
		{
			//SCREEN 6
			Console.WriteLine("El Balance de su cuenta es: \n" + String.Format("{0:C}", cuentaBancaria.GetBalance()));

			Console.WriteLine("El balance de su cuenta ha sido impreso.");
			AnotherTransation();
			return;
		}

		static void RetirarTarjeta()
		{
			//SCREEN 15
			Console.WriteLine("Por favor proceder a retirar la tarjeta.\n Gracias.");
			Console.ReadKey();
			return;
		}

		static void RealizarDeposito()
		{

			if (cajero.PermiteDeposito())
			{
				//SCREEN 7
				Console.WriteLine("Digite el monto. \n Los retiros deben ser multiplos de $10.");
				Decimal.TryParse(Console.ReadLine(), out decimal monto);
				InsertarDeposito(monto);
			}
			else
			{
				DepositoInactivo();
			}
			Console.ReadKey();
			return;
		}


		static void RealizarRetiro()
		{

			if (cajero.PermiteRetiro())
			{
				//SCREEN 7
				Console.WriteLine("Digite el monto. \nLos retiros deben ser multiplos de $10.");
				Decimal.TryParse(Console.ReadLine(), out decimal monto);

				if (EsMultiplo(monto, 10))
				{

					if (cajero.VerificarMontoDisponible(monto))
					{
						InsertarRetiro(monto);

					}
					else
					{
						CajeroSinMontoDisponible();

					}

             
				}
				else
				{
					MontoNoMultiplo();

				}

			}
			else
			{
				RetiroInactivo();
			}
			Console.ReadKey();
			return;
		}

		static void DepositoInactivo()
		{
			//SCREEN 12
			Console.WriteLine("Temporalmente no esta habilitado el proceso de deposito.");
			AnotherTransation();
		}

		static void RetiroInactivo()
		{
			//SCREEN 10
			Console.WriteLine("Temporalmente no esta habilitado el proceso de retiro.");
			AnotherTransation();
		}

		static void InsertarDeposito(decimal monto)
		{
			//SCREEN 13
			Console.WriteLine("Por favor proceder a insertar el deposito dentro del slot.");
			var nuevo_balance = cuentaBancaria.Depositar(monto);

			DisplayBalance();
			Console.ReadKey();
			return;
		}

		static void InsertarRetiro(decimal monto)
		{
			//SCREEN 11

			var balance = cuentaBancaria.GetBalance();

			if (balance < 10)
			{
				Console.WriteLine("No posee fondo suficiente para poder realizar retiros.");
				AnotherTransation();
				return;
			}


			if (balance >= monto)
			{

				Console.WriteLine("Su saldo se está actualizando. Por favor tome el efectivo del dispensador");

				var nuevo_balance = cuentaBancaria.Retirar(monto);

				DisplayBalance();
				Console.ReadKey();
				return;

			}
			else
			{

				CuentaSinBalanceSuficiente();
			}
		}
		static void CuentaSinBalanceSuficiente()
		{
			//SCREEN 8
			Console.WriteLine("Fondos insuficientes! \nPor favor ingrese una nueva cantidad");
			Decimal.TryParse(Console.ReadLine(), out decimal monto);
			RealizarRetiro();
            
		}

		static void CajeroSinMontoDisponible()
		{

			Console.WriteLine("El cajero no tiene fondo, para realizar la transaccion.");
			AnotherTransation();

		}

		static void MontoNoMultiplo()
		{
			//SCREEN 9
			Console.WriteLine("La máquina solo puede dispensar billetes de $10\n");
			Console.ReadKey();
			RealizarRetiro();
		}

		static bool EsMultiplo(decimal x, int n)
		{
			return (x % n) == 0;
		}
	}
}
