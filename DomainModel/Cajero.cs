using System;

namespace ATM.DomainModel
{
    public class Cajero : BaseModel
    {
        public decimal MontoDisponible { get; set; }
        public bool Atascado { get; set; }
    }
}