using System;

namespace ATM.DomainModel
{
    public class Tarjeta : BaseModel
    {
        public string PAN { get; set; }
        public string PIN { get; set; }
        public int CuentaId { get; set; }
        public virtual Cuenta Cuenta { get; set; }
    }
}