using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace ATM.DomainModel
{
    public class Cuenta : BaseModel
    {
        public string Cliente { get; set; }
        public string NoCuenta { get; set; }
        public decimal Balance { get; set; }

        [ForeignKey("Tarjeta")]
        public int TarjetaId { get; set; }
        public virtual Tarjeta Tarjeta { get; set; }
    }
}